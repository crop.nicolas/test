var commits = require('./api')


module.exports = function (app) {
  //Middleware pour autoriser la webapp
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.get('/commits', commits.commits)
    .get('/users', commits.users)

  app.get('*', function (req, res) {
    res.send('???', 404)
  })
}
