var express = require('express')
var config = require('./config')
var router = require('./routes')
var path = require('path')
var bodyParser = require('body-parser')

var app = express()

app.use(express.static(path.join(__dirname, '/static')))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(require('helmet')())

router(app)

app.listen(config.port)
console.log(`Application listening on ${config.port}`)
