var fs = require('fs')

module.exports = {
  commits (req, res) {
    if (!fs.existsSync('./datas/commits.json')) return res.send({error: 1, message: 'Pas de fichiers de commits'})
    var commits = require('./datas/commits.json')
    res.send(commits)
  },
  users (req, res) {
    if (!fs.existsSync('./datas/commits.json')) return res.send({error: 1, message: 'Pas de fichiers d\'utilisateurs'})
    var users = require('./datas/users.json')
    res.send(users)
  }
}
