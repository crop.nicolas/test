import Vue from 'vue'
import Vuex from 'vuex'
import config from './../serveur/config'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: null,
    commits: null,
    currentCommit: null
  },
  mutations: {
    SELECT: (state, v) => { state.currentCommit = v },
    SET_COMMITS: (state, c) => { state.commits = c },
    SET_USERS: (state, u) => { state.users = u }
  },
  getters: {
    selected: (state) => {
      if (!state.users) return null
      return state.users[state.currentCommit] || null
    },
    getUserById: (state) => {
      return function (id) {
        return state.users[id] || null
      }
    }
  },
  actions: {
    loadCommits: (store) => {
      return new Promise((resolve, reject) => {
        Vue.http.get(`http://localhost:${config.port}/commits`)
          .then(r => {
            if (r.body.error) return reject(new Error(r.body.message))
            store.commit('SET_COMMITS', r.body)
            resolve()
          })
      })
    },
    loadUsers: (store) => {
      return new Promise((resolve, reject) => {
        Vue.http.get(`http://localhost:${config.port}/users`)
          .then(r => {
            if (r.body.error) return reject(new Error(r.body.message))
            store.commit('SET_USERS', r.body)
            resolve()
          })
      })
    }
  }
})
